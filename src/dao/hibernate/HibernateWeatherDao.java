package dao.hibernate;

import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class HibernateWeatherDao {
    public List<LocationEntity> list() {
        List<LocationEntity> locations = new ArrayList<>();

        Session session = HibernateDaoUtils.getSessionFactory().openSession();
        Transaction transaction = null;

        try {
            // Begin a transaction
            transaction = session.beginTransaction();
            // Query the Locations
            locations = session.createQuery("from locations", LocationEntity.class).getResultList();
            // Commit the transaction
            transaction.commit();
        } catch (Exception e) {
            // If there are any exceptions, roll back the changes
            if (transaction != null) {
                transaction.rollback();
            }
            // Print the Exception
            e.printStackTrace();
        } finally {
            // Close the session
            session.close();
        }

        return locations;
    }

    // C - create aka. addLocation(...)
    public LocationEntity create(LocationEntity locationEntity) {

        Session session = HibernateDaoUtils.getSessionFactory().openSession();
        Transaction transaction = null;

        try {
            // Begin a transaction
            transaction = session.beginTransaction();
            // Save the Location
            Long savedLocationId = (Long) session.save(locationEntity);
            // Commit the transaction
            transaction.commit();

            locationEntity.setId(savedLocationId);
        } catch (Exception e) {
            // If there are any exceptions, roll back the changes
            if (transaction != null) {
                transaction.rollback();
            }
            // Print the Exception
            e.printStackTrace();
        } finally {
            // Close the session
            session.close();
        }

        return locationEntity;
    }

    // R - read aka. getLocationWeather(...)
    public LocationEntity read(Long id) {

        Session session = HibernateDaoUtils.getSessionFactory().openSession();
        Transaction transaction = null;
        LocationEntity locationEntity = null;

        try {
            // Begin a transaction
            transaction = session.beginTransaction();
            // Save the Location
            locationEntity = session.get(LocationEntity.class, id);
            // Commit the transaction
            transaction.commit();

        } catch (Exception e) {
            // If there are any exceptions, roll back the changes
            if (transaction != null) {
                transaction.rollback();
            }
            // Print the Exception
            e.printStackTrace();
        } finally {
            // Close the session
            session.close();
        }

        return locationEntity;
    }

    // U - update aka. saveLocationWeather(...)
    public LocationEntity update(LocationEntity locationEntity) {
        Session session = HibernateDaoUtils.getSessionFactory().openSession();
        Transaction transaction = null;

        try {
            // Begin a transaction
            transaction = session.beginTransaction();
            // Save the Location
            session.saveOrUpdate(locationEntity);
            // Commit the transaction
            transaction.commit();

        } catch (Exception e) {
            // If there are any exceptions, roll back the changes
            if (transaction != null) {
                transaction.rollback();
            }
            // Print the Exception
            e.printStackTrace();
        } finally {
            // Close the session
            session.close();
        }

        return locationEntity;
    }
}
