package services.mappers;
import dao.hibernate.LocationEntity;
import models.Location;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.stream.Collectors;

public class LocationMapper {
    public List<Location> fromList(List<LocationEntity> locationEntities) {
        return locationEntities.stream()
                .map(this::from)
                .collect(Collectors.toList());
    }

    public LocationEntity from(Location Location) {
        ModelMapper modelMapper = new ModelMapper();
        LocationEntity locationEntity = modelMapper.map(Location, LocationEntity.class);
        return locationEntity;
    }

    public Location from(LocationEntity locationEntity) {
        Location Location = new Location();
        if (locationEntity != null) {
            Location.setId(locationEntity.getId());
            Location.setCity(locationEntity.getCity());
            Location.setCountryCode(locationEntity.getCountryCode());
            Location.setLatitude(locationEntity.getLatitude());
            Location.setLongitude(locationEntity.getLongitude());
            Location.setTemperature(locationEntity.getTemperature());
            Location.setPressure(locationEntity.getPressure());
            Location.setHumidity(locationEntity.getHumidity());
            Location.setWindSpeed(locationEntity.getWindSpeed());
        }

        return Location;
    }
}
