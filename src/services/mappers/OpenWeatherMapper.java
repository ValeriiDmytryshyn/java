package services.mappers;

import external.api.openweather.models.current.*;
import models.Location;

public class OpenWeatherMapper {
    public Location from(OpenWeatherApiCurrentWeatherDataResponse weatherDataResponse) {
        Location Location = new Location();

        if (weatherDataResponse != null) {
            Coord weatherDataResponseCoord = weatherDataResponse.getCoord();
            if (weatherDataResponseCoord != null) {
                Location.setLatitude(weatherDataResponseCoord.getLat());
                Location.setLongitude(weatherDataResponseCoord.getLon());
            }

            Main weatherDataResponseMain = weatherDataResponse.getMain();
            if (weatherDataResponseMain != null) {
                Location.setTemperature(weatherDataResponseMain.getTemp());
                Location.setPressure(weatherDataResponseMain.getPressure());
                Location.setHumidity(weatherDataResponseMain.getHumidity());
            }

            Wind weatherDataResponseWind = weatherDataResponse.getWind();
            if (weatherDataResponseWind != null) {
                Location.setWindSpeed(weatherDataResponseWind.getSpeed());
            }

            Location.setCity(weatherDataResponse.getName());

            Sys weatherDataResponseSys = weatherDataResponse.getSys();
            if (weatherDataResponseSys != null) {
                Location.setCountryCode(weatherDataResponseSys.getCountry());
            }
        }

        return Location;
    }
}
