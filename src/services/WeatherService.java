package services;

import dao.hibernate.HibernateWeatherDao;
import dao.hibernate.LocationEntity;
import models.Location;
import services.mappers.LocationMapper;

import java.util.List;

public class WeatherService {
    private final HibernateWeatherDao hibernateWeathermanDao;
    private final LocationMapper locationMapper;

    public WeatherService(HibernateWeatherDao hibernateWeathermanDao, LocationMapper locationMapper) {
        this.hibernateWeathermanDao = hibernateWeathermanDao;
        this.locationMapper = locationMapper;
    }

    // L - list aka. listLocations()
    public List<Location> list() {
        List<LocationEntity> locationEntities = hibernateWeathermanDao.list();
        List<Location> Locations = locationMapper.fromList(locationEntities);
        return Locations;
    }

    // C - create aka. addLocation(...)
    public Location create(Location Location) {
        LocationEntity locationEntity = locationMapper.from(Location);
        LocationEntity createdLocationEntity = hibernateWeathermanDao.create(locationEntity);
        Location createdLocation = locationMapper.from(createdLocationEntity);

        return createdLocation;
    }

    // R - read aka. getLocationWeather(...)
    public Location read(Long id) {
        LocationEntity locationEntity = hibernateWeathermanDao.read(id);
        Location Location = locationMapper.from(locationEntity);
        return Location;
    }

    // U - update aka. saveLocationWeather(...)
    public Location update(Location Location) {

        LocationEntity locationEntity = locationMapper.from(Location);
        LocationEntity updatedLocationEntity = hibernateWeathermanDao.update(locationEntity);
        Location updatedLocation = locationMapper.from(updatedLocationEntity);

        return updatedLocation;
    }

    // D - delete aka. removeLocation(...)
    public void delete(Long id) {
        throw new UnsupportedOperationException("Operation not supported YET!");
    }
}
