package services;

import external.api.openweather.models.current.OpenWeatherApiCurrentWeatherDataResponse;
import external.api.openweather.services.OpenWeatherApiCurrentWeatherDataService;
import models.Location;
import models.WeatherDataRequest;
import services.mappers.OpenWeatherMapper;

import java.util.List;

public class WeatherManagerService {
    private final WeatherService weatherService;
    private final OpenWeatherApiCurrentWeatherDataService openWeatherApiCurrentWeatherDataService;
    private final OpenWeatherMapper openWeatherMapper;

    public WeatherManagerService(WeatherService weatherService,
                                 OpenWeatherApiCurrentWeatherDataService openWeatherApiCurrentWeatherDataService,
                                 OpenWeatherMapper openWeatherMapper) {
        this.weatherService = weatherService;
        this.openWeatherApiCurrentWeatherDataService = openWeatherApiCurrentWeatherDataService;
        this.openWeatherMapper = openWeatherMapper;
    }

    // L - list aka. listLocations()
    public List<Location> list() {
        List<Location> Locations = weatherService.list();
        return Locations;
    }

    // C - create aka. addLocation(...)
    public Location create(WeatherDataRequest WeatherDataRequest) {

        OpenWeatherApiCurrentWeatherDataResponse apiCurrentWeatherDataResponse =
                openWeatherApiCurrentWeatherDataService.getCurrentWeather(WeatherDataRequest);
        Location Location = openWeatherMapper.from(apiCurrentWeatherDataResponse);
        Location createdLocation = weatherService.create(Location);

        return createdLocation;
    }

    // R - read aka. getLocationWeather(...)
    public Location read(Long id) {
        Location readLocation = weatherService.read(id);
        return readLocation;
    }
}
