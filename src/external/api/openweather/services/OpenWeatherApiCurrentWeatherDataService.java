package external.api.openweather.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import external.api.openweather.models.current.OpenWeatherApiCurrentWeatherDataResponse;
import models.WeatherDataRequest;
import okhttp3.Call;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import java.io.IOException;

public class OpenWeatherApiCurrentWeatherDataService {
    private static final String SERVER_URL = "http://api.openweathermap.org/data/2.5/weather";

    public OpenWeatherApiCurrentWeatherDataResponse getCurrentWeather(WeatherDataRequest weatherDataRequest) {

        HttpUrl.Builder urlBuilder = HttpUrl.parse(SERVER_URL).newBuilder();
        urlBuilder.addQueryParameter("appid", "d2637e7f85faf82d4f6ddba63038faf2");

        if (weatherDataRequest.getCity() != null) {
            urlBuilder.addQueryParameter("q", weatherDataRequest.getCity() + "," + weatherDataRequest.getCountryCode());
            urlBuilder.addQueryParameter("units", "metric");
        } else {
            urlBuilder.addQueryParameter("lat", String.valueOf(weatherDataRequest.getLatitude()));
            urlBuilder.addQueryParameter("lon", String.valueOf(weatherDataRequest.getLongitude()));
        }

        HttpUrl httpUrl = urlBuilder.build();

        Request request = new Request.Builder()
                .url(httpUrl)
                .get()
                .build();

        OkHttpClient client = new OkHttpClient();
        Call call = client.newCall(request);

        try {
            Response response = call.execute();
            ResponseBody responseBody = response.body();

            String bodyString = null;
            if (responseBody != null) {
                bodyString = responseBody.string();
            }

            OpenWeatherApiCurrentWeatherDataResponse weatherDataResponse = deserializeResponse(bodyString);

            return weatherDataResponse;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    OpenWeatherApiCurrentWeatherDataResponse deserializeResponse(String body) {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            OpenWeatherApiCurrentWeatherDataResponse weatherDataResponse =
                    objectMapper.readValue(body, OpenWeatherApiCurrentWeatherDataResponse.class);

            return weatherDataResponse;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }
}
