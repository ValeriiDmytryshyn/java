package controller;

import models.Location;
import models.WeatherDataRequest;
import services.WeatherManagerService;
import utils.Utils;

import java.util.List;
import java.util.Scanner;

public class WeatherController {
    private final Scanner scanner;
    private final services.WeatherManagerService WeatherManagerService;

    public static final String CLI_GLOBAL_CHOOSE_ACTION_INFO_TEXT =
            "Choose action:\n" +
                    "1 - List Locations\n" +
                    "2 - Add Location\n" +
                    "3 - Get Location Weather\n" +
                    "q - Quit App\n";

    public WeatherController(Scanner scanner, WeatherManagerService WeatherManagerService) {
        this.scanner = scanner;
        this.WeatherManagerService = WeatherManagerService;
    }

    public void mainCLiLoop() {
        System.out.println("Welcome in the Weatherman App!");
        System.out.println(CLI_GLOBAL_CHOOSE_ACTION_INFO_TEXT);

        String text;
        while (!(text = scanner.nextLine()).equals("q")) {

            int chosenAction = -1;
            try {
                chosenAction = Integer.parseInt(text);
            } catch (Exception e) {
            }

            switch (chosenAction) {
                case 1: {
                    // list locations
                    List<Location> locations = WeatherManagerService.list();
                    System.out.println("All Locations:");
                    locations.forEach(System.out::println);
                    break;
                }
                case 2: {
                    // add location
                    System.out.println("Adding new Location:");

                    String city = Utils.textInputWait(scanner, "Enter City name (e.g.: Kiev):");
                    String countryCode = Utils.textInputWait(scanner, "Enter Country Code (e.g.: UA):");;
                    WeatherDataRequest WeatherDataRequest = new WeatherDataRequest(city, countryCode);

                    Location Location = WeatherManagerService.create(WeatherDataRequest);
                    System.out.println("Added Location data:");
                    System.out.println(Location);
                    break;
                }
                case 3: {
                    // get location weather
                    Long locationId = Utils.numberInputWait(scanner, "Enter Location ID (TIP: Use Command \"1 - List Locations\" ):");

                    Location locationWeatherData = WeatherManagerService.read(locationId);
                    System.out.println("Location Weather Data:");
                    System.out.println(locationWeatherData);
                    break;
                }
                default: {
                    System.out.println("Unknown Action!");
                    break;
                }
            }

            System.out.println(CLI_GLOBAL_CHOOSE_ACTION_INFO_TEXT);
        }

        System.out.println("Good bye!");
    }
}
