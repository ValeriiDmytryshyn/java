import controller.WeatherController;
import dao.hibernate.HibernateWeatherDao;
import external.api.openweather.services.OpenWeatherApiCurrentWeatherDataService;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import services.WeatherManagerService;
import services.WeatherService;
import services.mappers.LocationMapper;
import services.mappers.OpenWeatherMapper;

import java.util.Scanner;

public class WeatherApplication {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Logger.getRootLogger().setLevel(Level.OFF);

        LocationMapper locationMapper = new LocationMapper();
        OpenWeatherMapper openWeatherMapper = new OpenWeatherMapper();

        HibernateWeatherDao HibernateWeatherDao = new HibernateWeatherDao();
        OpenWeatherApiCurrentWeatherDataService openWeatherApiCurrentWeatherDataService = new OpenWeatherApiCurrentWeatherDataService();

        WeatherService WeatherService = new WeatherService(HibernateWeatherDao, locationMapper);
        WeatherManagerService WeatherManagerService = new WeatherManagerService(
                WeatherService, openWeatherApiCurrentWeatherDataService, openWeatherMapper);
        WeatherController WeatherController = new WeatherController(scanner, WeatherManagerService);
        WeatherController.mainCLiLoop();
    }
}
