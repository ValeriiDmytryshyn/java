package models;

public class WeatherDataRequest {
    private String city;
    private String countryCode;

    private double latitude;
    private double longitude;

    private int days = 1;

    public WeatherDataRequest(String city, String countryCode) {
        this.city = city;
        this.countryCode = countryCode;
    }

    public WeatherDataRequest(String city, String countryCode, int days) {
        this.city = city;
        this.countryCode = countryCode;
        this.days = days;
    }

    public WeatherDataRequest(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public WeatherDataRequest(double latitude, double longitude, int days) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.days = days;
    }

    public String getCity() {
        return city;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public int getDays() {
        return days;
    }

    @Override
    public String toString() {
        return "WeatherDataRequest{" +
                "city='" + city + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", days=" + days +
                '}';
    }
}
